﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class onCatCollision : MonoBehaviour {

    public AudioMixerSnapshot duckingSnapshot;
    public AudioMixerSnapshot defaultSnapshot;

    private AudioSource catSource;
    private bool catColliding;

    public float f_volMin = 0.8f;
    public float f_volMax = 1.0f;

    public float f_pitchMin = 0.9f;
    public float f_pitchMax = 1.1f;

    // Use this for initialization
    void Start()
    {
        catColliding = false;
        catSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q) && catColliding)
        {
            duckingSnapshot.TransitionTo(.3f);
            float fvolRandom = Random.Range(f_volMin, f_volMax);
            catSource.pitch = Random.Range(f_pitchMin, f_pitchMax);
            catSource.volume = fvolRandom;
            catSource.Play();
        }
        if (!catSource.isPlaying)
        {
            defaultSnapshot.TransitionTo(.3f);
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        catColliding = true;
    }
    private void OnTriggerExit(Collider other)
    {
        catColliding = false;
    }
}
