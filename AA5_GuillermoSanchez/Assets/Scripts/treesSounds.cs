﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;




public class treesSounds : MonoBehaviour
{

    [System.Serializable]
    public class OwlsGroup
    {
        public List<AudioClip> audioClips;
    }

    private AudioSource audioComponent;
    public OwlsGroup owlsGroup;
    

    public float f_volMin = 0.4f;
    public float f_volMax = 0.8f;
    public float f_pitchMin = 0.9f;
    public float f_pitchMax = 1.1f;

    // Use this for initialization
    void Start()
    {
        audioComponent = GetComponent<AudioSource>();
    }


    // Update is called once per frame
    void Update()
    {
        if (!audioComponent.isPlaying)
        {
            audioComponent.pitch = Random.Range(f_pitchMin, f_pitchMax);
            audioComponent.volume = Random.Range(f_volMin, f_volMax);
            audioComponent.clip = owlsGroup.audioClips[Random.Range(0, owlsGroup.audioClips.Count)];
            audioComponent.Play();
        }
    }
}
