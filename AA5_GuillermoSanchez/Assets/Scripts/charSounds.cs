﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;




public class charSounds : MonoBehaviour {

    [System.Serializable]
    public class FootstepGroup
    {
        public List<AudioClip> audioClips;
    }

    private AudioSource audioComponent;
    private FootstepGroup footstepGroup;

    public List<FootstepGroup> footsteps;

    private int i_surface;
    private string s_surfaceString;

    public float f_volMin = 0.8f;
    public float f_volMax = 1.0f;
    public float f_pitchMin = 0.9f;
    public float f_pitchMax = 1.1f;

    // Use this for initialization
    void Start () {
        audioComponent = GetComponent<AudioSource>();
        footstepGroup = footsteps[0];
    }

    void Footsteps()
    {
        audioComponent.pitch = Random.Range(f_pitchMin, f_pitchMax);
        audioComponent.volume = Random.Range(f_volMin, f_volMax);
        audioComponent.clip = footstepGroup.audioClips[Random.Range(0, footstepGroup.audioClips.Count)];
        audioComponent.Play();
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if(s_surfaceString != hit.transform.tag)
        {
            switch (hit.transform.tag)
            {
                case "grass":
                    {
                        f_volMin = 0.8f;
                        f_volMax = 1.0f;
                        i_surface = 0;
                    }
                    break;
                case "soil":
                    {
                        f_volMin = 0.1f;
                        f_volMax = 0.2f;
                        i_surface = 1;
                    }
                    break;
                default:
                    {
                        f_volMin = 0.8f;
                        f_volMax = 1.0f;
                        i_surface = 0;
                    }
                    break;
            }
            s_surfaceString = hit.transform.tag;
            footstepGroup = footsteps[i_surface];
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
