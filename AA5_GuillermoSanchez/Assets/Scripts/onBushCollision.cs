﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onBushCollision : MonoBehaviour {

    private AudioSource bushSource;

    public float f_volMin = 4f;
    public float f_volMax = 4.2f;

    public float f_pitchMin = 0.9f;
    public float f_pitchMax = 1.1f;

    // Use this for initialization
    void Start()
    {
        bushSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        float fvolRandom = Random.Range(f_volMin, f_volMax);
        bushSource.pitch = Random.Range(f_pitchMin, f_pitchMax);
        bushSource.volume = fvolRandom;
        bushSource.Play();
    }
}
