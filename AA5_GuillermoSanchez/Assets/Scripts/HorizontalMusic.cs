﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalMusic : MonoBehaviour {

    public AudioSource[] sourceArray = new AudioSource[4];
    public AudioSource forestMusic;
    public AudioSource desertMusic;
    //public AudioSource transitionStinger;

    public float fadeInSpeed;
    public float fadeOutSpeed;


    // Use this for initialization
    void Start () {
        sourceArray = GetComponents<AudioSource>();
        forestMusic = sourceArray[1];
        desertMusic = sourceArray[2];
        //transitionStinger = sourceArray[3];

        fadeInSpeed = 0.01f;
        fadeOutSpeed = 0.05f;
        forestMusic.volume = 0.3f;
        desertMusic.volume = 0.0f;
	}

    static IEnumerator FadeOut(float fadeOutSpeed, AudioSource forestMusic)
    {
        while (forestMusic.volume > 0.01f)
        {
            forestMusic.volume -= fadeOutSpeed;
            yield return new WaitForSeconds(0.1f);
        }
        forestMusic.volume = 0.0f;
        forestMusic.Stop();
    }

    static IEnumerator FadeIn(float fadeInSpeed, AudioSource desertMusic)
    {
        desertMusic.volume = 0.0f;

        while (desertMusic.volume < 0.3f)
        {
            desertMusic.volume += fadeInSpeed;
            yield return new WaitForSeconds(0.1f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("changeZone"))
        {
            //transitionStinger.Play();
            StartCoroutine(FadeOut(fadeOutSpeed, forestMusic));
            StartCoroutine(FadeIn(fadeInSpeed, desertMusic));
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
